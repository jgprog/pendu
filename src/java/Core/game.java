/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Core;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author jessygiacomoni
 */
public class game
{

    //Listener

    //Map player
    private Map<String, player> _getPlayer;
    
    //Know if this part it's ready
    private boolean _gameIsReady;
    
    
    public game() 
    {
        _getPlayer  = new ConcurrentHashMap<String, player>();
        _gameIsReady = false;
    }
    
    public boolean gameIsReady()
    {
        return _gameIsReady;
    }
    
    public void addPlayer(String namePlayer)
    {
        _getPlayer.put(namePlayer, new player(namePlayer));
        if(_getPlayer.size() > 1)
        {
            //updateObservateur() ;
        }
    }

}
