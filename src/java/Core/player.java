/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Core;

import java.util.ArrayList;

/**
 *
 * @author jessygiacomoni
 */
public class player 
{
    private final String _playerName;
    private int _nbCoups;
    private ArrayList _letterChoosen;
    private char[] _WordCoded;
    private int _nbCoupsFaits;
    
    private final dictionnary _dico;
    
    public player(String namePlayer, dictionnary dico)
    {
        _playerName = namePlayer;
        _dico = dico;
        _nbCoupsFaits = 0;
        _nbCoups = 8;
        _letterChoosen = new ArrayList();
        _WordCoded = _dico.getWordHidden().clone();
    }

    public player(String namePlayer) 
    {
        _playerName = namePlayer;
        _nbCoupsFaits = 0;
        _nbCoups = 8;
        _letterChoosen = new ArrayList();
        _dico = null;
    }
    
    public int  getNbCoups()
    {
        return _nbCoups;
    }
    
    public int getCoupsFait()
    {
        return _nbCoupsFaits;
    }
    
    public String getName()
    {
        return _playerName;
    }
    
    public ArrayList lettreChoosen()
    {
        return _letterChoosen;
    }
    
    
    private void WhereIsStringToDispay(char letter)
   {
       ArrayList listResult = _dico.whatIsPositionOfThisLetterInWord(letter);
       if(!listResult.isEmpty())
       {
           //Remplacement des lettres
           for (Object listResult1 : listResult) 
           {
               _WordCoded[(int) listResult1] =  _dico.getWordToFind().clone()[(int) listResult1];
           }
       }
   }
    
    
    public char[] searchLetter(char letterToFindInWord)
    {
        if (!_letterChoosen.contains(letterToFindInWord)) 
        {
            boolean add = _letterChoosen.add(letterToFindInWord);
            
            WhereIsStringToDispay(letterToFindInWord);
            if (!searchTab(letterToFindInWord)) 
            {
                _nbCoups -= 1;
                _nbCoupsFaits += 1;
            }
        }
        return _WordCoded;
    }
    
    private boolean searchTab(char letter)
    {
        int i =0;
        boolean ret = false;
        while( i < _dico.getWordToFind().length && letter != _dico.getWordToFind()[i])
        {
            i++;
        }
        if (i < _dico.getWordToFind().length)
        {
            ret = true;
        }
        return ret;
    }
    
    
}
