/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Core;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author jessygiacomoni
 */
public class file 
{
    private String _path;
        
    public file(String path)
    {
        _path = path;
    }
    
    public int nbLineInTxtFile() throws FileNotFoundException, IOException
    {
        BufferedReader fichier = new BufferedReader(new FileReader(_path));
        String ligne;
        int maxLine = 0;
        while ((ligne = fichier.readLine()) != null) 
        {
            maxLine++;
        }
        fichier.close();
        return maxLine;
    }
    
    public String aWordWithIndice(int indice) throws FileNotFoundException, IOException
    {
        BufferedReader fichier = new BufferedReader(new FileReader(_path));
        int nbLine = 0;
        String ligne;
        String WordReturn = null;
        
        while ((ligne = fichier.readLine()) != null) 
        {
           if(nbLine == indice)
           {
               WordReturn = ligne;
           }
            nbLine++;
        }
        
        fichier.close();
        return new String(WordReturn.trim().getBytes(),"UTF-8");
    }
    
}
