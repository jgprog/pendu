package Core;

import java.io.FileNotFoundException;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.io.FileReader;
import java.io.IOException;
import java.io.*;

public class dictionnary {

    private char[] _chooseWord;
    private char[] _stringToDisplay;
    private int _nbCoups = 8;

    private file _file;
    
    public dictionnary() throws IOException
    {
        _file = new file("/Users/jessygiacomoni/NetBeansProjects/Pendu/src/java/Core/liste_francais.txt");
        startGame();
        
    }


    
    
    //Accessors
    public int nbCoups() {
        return _nbCoups;
    }

    public char[] getWordToFind() 
    {
        return _chooseWord;
    }

    public char[] getWordHidden() {
        return _stringToDisplay;
    }

    //Public methods
    public void startGame() throws IOException 
    {
        _chooseWord = randomWord();
        _stringToDisplay = new char[_chooseWord.length];
        _stringToDisplay[0] = _chooseWord[0];

        for (int i = 1; i < _chooseWord.length; i++) {
            _stringToDisplay[i] = '_';
        }
    }

    public ArrayList whatIsPositionOfThisLetterInWord(char letter) {
        ArrayList retArrayPositionLetterInWord = new ArrayList();
        for (int i = 1; i < _chooseWord.length; i++) {
            if (letter == _chooseWord[i]) {
                retArrayPositionLetterInWord.add(i);
            }
        }
        return retArrayPositionLetterInWord;
    }

    public char[] WhereIsStringToDispay(char letter) {
        ArrayList listResult = whatIsPositionOfThisLetterInWord(letter);
        if (!listResult.isEmpty()) {
            //Remplacement des lettres
            for (Object listResult1 : listResult) {
                _stringToDisplay[(int) listResult1] = _chooseWord[(int) listResult1];
            }
        }
        return _stringToDisplay;
    }

    //Private methods
    private char[] randomWord() throws IOException 
    {
        return chooseWordInTxtFile().toCharArray();
    }

    
    private String chooseWordInTxtFile() throws FileNotFoundException, IOException 
    {
        int random = (int) (Math.random() * _file.nbLineInTxtFile());
        return _file.aWordWithIndice(random);
    }
    
    
    
        //For UNIT  TEST !!!!!
    public void chooseWordToFind(char [] word) 
    {
         _chooseWord = word;
        _stringToDisplay = new char[_chooseWord.length];
        _stringToDisplay[0] = _chooseWord[0];

        for (int i = 1; i < _chooseWord.length; i++) {
            _stringToDisplay[i] = '_';
        }
    }
    //End UnitTest
}
