/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import Core.dictionnary;
import Core.game;
import Core.player;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javaBeanPackage.javaBeans;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author jessygiacomoni
 */
@WebServlet(urlPatterns = {"/multiPlayer"})
public class multiPlayer  extends HttpServlet
{

    dictionnary _dico;
  
    
    String _playerNameSession;
    
    Map<String, player> _getPlayer;
    String _stringNameWinner =null;
    HttpServletResponse _response ;
    
    private final game _game;
    
    public multiPlayer() throws IOException
    {
        _dico = new dictionnary();

        //Init game
        _dico.startGame();
        //EndInit
        _getPlayer  = new ConcurrentHashMap<>();
        
        //Game
        _game = new game();
    }
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @SuppressWarnings("empty-statement")
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {  
        String url = "";
        javaBeans beans = new javaBeans();
        
        if(_getPlayer.size() > 1)
        {
            String letter = request.getParameter("letter");
            
            String inter = request.getParameter("JoueurName");
            player _gamer;
            if(inter != null)
            {
                _gamer = _getPlayer.get(inter);
            }
            else
            {
                _gamer = _getPlayer.get(_playerNameSession);
            }

            //If - else supprime _dico.getwordHidden dans le cstr
            char[] stringToDisplay = new char[_dico.getWordHidden().length];
            if(letter == null)
            {
                stringToDisplay = _dico.getWordHidden();
            }
            else
            {  
                stringToDisplay = _gamer.searchLetter(letter.charAt(0));
            }
            //Word
            beans.setWordHidden(stringToDisplay);
            beans.setWord(_dico.getWordToFind());

            
            if(_gamer.getNbCoups() == 0)
            {
                url = "gameLoose.jsp";
                restart();
            }
            else if(!Arrays.equals(stringToDisplay, _dico.getWordToFind()))
            {
                if(letter != null)
                {
                    url = "lettreChoisie.jsp";
                    beans.setLettreChoisies(_gamer.lettreChoosen());
                    beans.setNbCoups(_gamer.getNbCoups());
                    beans.setNbCoupsFait(_gamer.getCoupsFait());
                }
                else
                {
                   url = "firstPage.jsp";
                }    
            }
            else
            {
                if(_stringNameWinner == null)
                {
                    _stringNameWinner = _gamer.getName();
                }
                int nbcoupsfaitavantDeTrouver =  8 - _gamer.getNbCoups();
                beans.setnbCoupsPourtrouver(nbcoupsfaitavantDeTrouver);
                beans.setWinnerName(_stringNameWinner);
                url = "gameSuccess.jsp";
                restart();
            }
        }
        else
        {
            url = "waitSecondPlayer.jsp";
        }
        
        beans.setFormAction("./multiPlayer");
        
        //SendRequest
        request.setAttribute("Beans", beans);
        RequestDispatcher dispatch = request.getRequestDispatcher(url); 
        dispatch.forward(request,response);
        
    }
    
    private void restart() throws IOException
    {
        //Init game
        _dico.startGame();
        _getPlayer  = new ConcurrentHashMap<>();
        _stringNameWinner = null;
    }
    
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
        _response = response;
        //_game.addObservateur(this);
        setSession(request,response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
        _response = response;
        setSession(request,response);
    }

    private void setSession(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        HttpSession session = request.getSession();
        
        String PlayerName = request.getParameter("JoueurName");
        _playerNameSession = (String) session.getAttribute("JoueurName");
        if(PlayerName != null)
        {
            session.setAttribute("JoueurName", PlayerName);
            
            if(_getPlayer.get(PlayerName) == null)
            {
                _getPlayer.put(PlayerName, new player(PlayerName,_dico));
            }
             processRequest(request, response);
        }
        else if(_playerNameSession != null)
        {
            if(_getPlayer.get(_playerNameSession) == null)
            {
                _getPlayer.put(_playerNameSession, new player(_playerNameSession,_dico));
            }
            processRequest(request, response);
        }
        else
        {
            PrintWriter out = response.getWriter();

            RequestDispatcher dispatch = request.getRequestDispatcher("enterName.jsp"); 
            dispatch.forward(request,response);
        }
    }
    
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() 
    {
        return "Short description";
    }// </editor-fold>


}
