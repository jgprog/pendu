/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControllerSolo;

import Core.dictionnary;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import javaBeanPackage.javaBeans;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.RequestDispatcher;

/**
 *
 * @author jessygiacomoni
 */
@WebServlet(urlPatterns =
{
    "/controller"
})
public class controller extends HttpServlet
{

    dictionnary _dico;
    char[] _stringToDisplay;
    char[] _stringOld;
    int _nbCoups;
    int _nbCoupsFait;
    ArrayList _letterChoosen;

    String _playerNameSession;

    public controller() throws IOException
    {
        _dico = new dictionnary();

        //Init game
        _dico.startGame();
        _nbCoups = 8;
        _nbCoupsFait = 0;
        _letterChoosen = new ArrayList();
        //EndInit
    }

    public dictionnary getDictionnary()
    {
        return _dico;
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @SuppressWarnings("empty-statement")
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String url = "";
        javaBeans beans = new javaBeans();
        
        String letter = request.getParameter("letter");
        if (letter == null)
        {
            _stringToDisplay = _dico.getWordHidden();
        } else
        {
            _stringToDisplay = _dico.WhereIsStringToDispay(letter.charAt(0));
        }

        //Word
        beans.setWordHidden(_stringToDisplay);
        beans.setWord(_dico.getWordToFind());
        
        //Page Ou c'est terminé mais Pas trouvé
        if (_nbCoups == 0)
        {
            restart();
            //GO TO gameLoose
            url = "gameLoose.jsp";
        } 
        else if (!Arrays.equals(_stringToDisplay, _dico.getWordToFind()))
        {
            if (letter != null)
            {
                if (_letterChoosen.contains(letter.charAt(0)))
                {
                    // GO TO LettreChoisiMaisDejaPresente
                    url = "lettreChoisiMaisDejaPresente.jsp";
                } else
                {
                    _letterChoosen.add(letter.charAt(0));
                    
                    if (!searchTab(letter.charAt(0)))
                    {
                        _nbCoups -= 1;
                        _nbCoupsFait += 1;
                    }
                   
                    //GO TO LettreChoisie
                    url = "lettreChoisie.jsp";
                }
            } else
            {
                //Init game. 
                // GO TO INIT GAME
                url = "firstPage.jsp";
            }
        } //Dans le cas ou on a trouvé le mot. Le jeu est terminé.
        else
        {
            int nbcoupsfaitavantDeTrouver = 8 - _nbCoups;
            beans.setnbCoupsPourtrouver(nbcoupsfaitavantDeTrouver);
            restart();
            //GO TO gameSuccess
            url = "gameSuccess.jsp";
        }
      
        
        //Coups , Lettres choisies
        beans.setLettreChoisies(_letterChoosen);
        beans.setNbCoups(_nbCoups);
        beans.setNbCoupsFait(_nbCoupsFait);
        
        //Send To Form
        beans.setFormAction("./controller");
        
        //SendRequest
        request.setAttribute("Beans", beans);
        RequestDispatcher dispatch = request.getRequestDispatcher(url); 
        dispatch.forward(request,response);
    }

    private boolean searchTab(char letter)
    {
        int i = 0;
        boolean ret = false;
        while (i < _dico.getWordToFind().length && letter != _dico.getWordToFind()[i])
        {
            i++;
        }
        if (i < _dico.getWordToFind().length)
        {
            ret = true;
        }
        return ret;
    }

    private void restart() throws IOException
    {
        //Init game
        _dico.startGame();
        _nbCoups = 8;
        _nbCoupsFait = 0;
        _letterChoosen = new ArrayList();

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>

}
