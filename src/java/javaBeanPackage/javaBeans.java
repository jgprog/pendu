/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaBeanPackage;

import ControllerSolo.controller;
import java.io.IOException;
import java.util.Arrays;
import java.util.ArrayList;

/**
 *
 * @author jessygiacomoni
 */
public class javaBeans {

    controller _contro;

    String _getWord;
    int _nbCoups;
    int _nbCoupsPourTrouver;
    String _wordHidden;
    int _nbCoupsFait;
    String _lettreChoisies;
    String _winnerName;
    String _formAction;
    
    public void setWord(char[] word) {
        String res = "";
        for (int i = 0; i < word.length; i++)
        {
            res += word[i] + "&nbsp;";
        }
        _getWord = res;
    }

    public String getWord() 
    {
        return _getWord;
    }
    
    public void setWordHidden(char[] word)
    {
         String res = "";
        for (int i = 0; i < word.length; i++)
        {
            res += word[i] + "&nbsp;";
        }
        _wordHidden = res;
    }
    
    public String getWordHidden()
    {
        return _wordHidden;
    }
    public void setNbCoups(int nbcoups)
    {
        _nbCoups = nbcoups;
    }
    
    public int getNbCoups()
    {
        return _nbCoups;
    } 
    
    public int getnbCoupsPourtrouver()
    {
        return _nbCoupsPourTrouver;
    }

    public void setnbCoupsPourtrouver(int nbCoups)
    {
        _nbCoupsPourTrouver = nbCoups;
    }
    
    public int getNbCoupsFait()
    {
        return _nbCoupsFait;
    }
    
    public void setNbCoupsFait(int nbcoups)
    {
        _nbCoupsFait = nbcoups;
    }
    
    public String getLettreChoisies()
    {
        return _lettreChoisies;
    }
    
    public void setLettreChoisies(ArrayList lettreChoisies)
    {
        _lettreChoisies = lettreChoisies.toString();
    }
    
    
    public void setWinnerName(String name)
    {
        _winnerName = name;
    }
    
    public String getWinnerName()
    {
        return _winnerName;
    }
    
    public String getFormAction()
    {
         return _formAction;
    }
    
    public void setFormAction(String url)
    {
        _formAction = url;
    }
}
