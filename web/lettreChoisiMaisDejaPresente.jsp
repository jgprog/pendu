<%-- 
    Document   : penduSolo
    Created on : 31 mars 2014, 16:06:23
    Author     : jessygiacomoni
--%>

<%@page import="javaBeanPackage.javaBeans"%>
<% javaBeans Beans = (javaBeans) request.getAttribute("Beans"); %>

<jsp:include page="HeaderPage.jsp" />
                <br />
                <p style="color:white"><%= Beans.getWord()%></p>
                <div class="col-md-6"> 
                     <p>Le mot � trouver est sous la forme : <%= Beans.getWordHidden()%></p>

                    <form Method="POST" Action="<%= Beans.getFormAction() %>">
                        <input type="textarea" class="form-control" name="letter" maxlength="1" style="width:200px" placeholder="Saisir ici la lettre" /><br />
                        <input type=submit class="btn btn-default" value=Envoyer>
                    </form>

                </div>
                <div class="col-md-6">
                    <div class="alert alert-danger">Tu as d�j� saisi cette lettre</div>
                    
                    <img src="./img/<%= Beans.getNbCoupsFait()%>.gif"/>
                    <br /> <br /><p> Tu as encore <%= Beans.getNbCoups() %> coups
                        <br />Les lettres d�j� saisies :  <%= Beans.getLettreChoisies()%></p>
                </div>
            </div>
        </div>          
    </body>
</html>