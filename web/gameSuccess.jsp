<%-- 
    Document   : gameLoose
    Created on : 31 mars 2014, 18:45:21
    Author     : jessygiacomoni
--%>

<%@page import="javaBeanPackage.javaBeans"%>
<% javaBeans Beans = (javaBeans) request.getAttribute("Beans"); %>

    
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="./bootstrap/css/bootstrap.min.css" />
        <title>Jeu du pendu</title>
        <script type="text/javascript" src="js/jQuery.js" ></script>
        <script type="text/javascript" src="js/Fire.js" ></script>
    </head>
    <body style="color:white;background-color:black">
        <div class="container" style="color:white;background-color:black">
            <div class="page-header">
                <h1>Bienvenue sur le jeu du Pendu !<br />
                    <small> Attention ne vous pendez pas !</small>
                </h1>
            </div>



            <div class="row">
                <br />
                    <% if(Beans.getWinnerName() != null ) 
                    { %>
<p> Le Winner est.... <%= Beans.getWinnerName() %> </p>
                    <% } %>
                        <div class="alert alert-success"> Tu es trop fort tu as trouv� !! En <%= Beans.getNbCoups() %> coup(s)</div>
                       <form Method="POST" Action="<%= Beans.getFormAction()%>">
                        <input type=submit class="btn btn-default" value=Restart>
                       </form>
                    </div>
                       
         </div>
     </body>
</html>
         
