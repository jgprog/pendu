/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Core;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author jessygiacomoni
 */
public class dictionnaryTest 
{

    private char[] _wordtoForTest;
    
    @BeforeClass
    public static void setUpClass() throws Exception 
    {
        
    }

    @AfterClass
    public static void tearDownClass() throws Exception
    {
    }
    
    @Before
    @SuppressWarnings("empty-statement")
    public void setUp() 
    {
         char[] inter =  {'c','o','u','c','o','u'};
         _wordtoForTest = inter;
    }
    
    @After
    public void tearDown() 
    {
        
    }

    /**
     * Test of WhereIsStringToDispay method, of class dictionnary.
     */
    @Test
    public void testWhereIsStringToDispay() throws IOException 
    {
        System.out.println("WhereIsStringToDispay");
        char letter = 'o';
        dictionnary instance = new dictionnary();
        instance.chooseWordToFind(_wordtoForTest);
        char[] expResult = {'c','o','_','_','o','_'};
        char[] result = instance.WhereIsStringToDispay(letter);
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of whatIsPositionOfThisLetterInWord method, of class dictionnary.
     */
    @Test
    public void testWhatIsPositionOfThisLetterInWord() throws IOException 
    {
        System.out.println("whatIsPositionOfThisLetterInWord");
        char letter = 'o';
        dictionnary instance = new dictionnary();
        instance.chooseWordToFind(_wordtoForTest);
        ArrayList expResult = null;
        ArrayList result = instance.whatIsPositionOfThisLetterInWord(letter);
        assertEquals(2, result.size());
    }
    
    @Test
    public void TestFirstValidation() throws IOException
    {
        System.out.println("whatIsPositionOfThisLetterInWord");
        char letter = 'o';
        dictionnary instance = new dictionnary();
        instance.chooseWordToFind(_wordtoForTest);
        char[] result = instance.WhereIsStringToDispay(letter);
        assertEquals(6, result.length);  
        char[] expResultString = {'c','o','_','_','o','_'};
        assertArrayEquals(expResultString, result);
    }
    
}
